package com.vendorapp.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.vendorapp.dtos.Dto_GetXSubmission;
import com.vendorapp.dtos.Dto_XSubmission;
import com.vendorapp.net.Connectable;
import com.vendorapp.net.TransmissionServiceHandler;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ViewCommoditiesActivity extends ListActivity implements Connectable{

	String vid;
	Gson gson = new Gson();
	List<Dto_GetXSubmission> subs;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_cmdtyz);
		setTitle("Commodities");
		
		if(haveInternet(this))
			load();
		else
			Alert("No Connection", "No internet connection. Make sure you are connected to the internet");				
	
	}
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		String[] options = {"Edit", "Back"};
		final int pos = position;
		AlertDialog.Builder opts = new AlertDialog.Builder(ViewCommoditiesActivity.this);
		opts.setItems(options, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				switch (which) {
				case 0:
					Intent out = new Intent(ViewCommoditiesActivity.this, UpdatePriceActivity.class);
					out.putExtra("id", subs.get(pos).getId());
					out.putExtra("cid", subs.get(pos).getSubmsn().getCmdty().getId());
					out.putExtra("mid", subs.get(pos).getSubmsn().getMkt().getId());
					out.putExtra("p", subs.get(pos).getSubmsn().getPrice());
					out.putExtra("u", subs.get(pos).getSubmsn().getUnit());
					startActivity(out);
					finish();
					break;
				
				case 1:
				    dialog.cancel();	
					break;	

				default:
					break;
				}
			}
		});
		opts.show();
	}

	private void load(){
		
		Intent in = getIntent();
		vid = in.getStringExtra("vid");
		
		new CommodityLoader().execute();
	
	}
	
	private class CommodityBody {
		public String name, id, price, unit, mkt;
				
		public CommodityBody(String name, String id, String price, String mkt, String unit) {
			this.name = name;
			this.id = id;		
			this.unit = unit;
			this.price = price;
			this.mkt = mkt;
		}
		
	}
	
	public class CommodityAdapter extends ArrayAdapter<CommodityBody> {

		public CommodityAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.cmdty_lay, null);
			}
						
			TextView name = (TextView) convertView.findViewById(R.id.CMDTY_NAME);
			name.setText(getItem(position).name);
			
			TextView mkt = (TextView) convertView.findViewById(R.id.CMDTY_MARKET);
			mkt.setText(getItem(position).mkt);
			
			TextView price = (TextView) convertView.findViewById(R.id.CMDTY_PRICE);
			price.setText(getItem(position).price);
			
			TextView unit = (TextView) convertView.findViewById(R.id.CMDTY_UNITS);
			unit.setText(getItem(position).unit);
			
			return convertView;
		}
	}

    private class CommodityLoader extends AsyncTask<String, String, String> {
    	
    	ProgressDialog dialog;
    	
    	@Override
    	protected void onPreExecute(){
    	     dialog = ProgressDialog.show(ViewCommoditiesActivity.this, "Loading","Wait...", true);
    	        //do initialization of required objects objects here                
    	};
         
		@Override
		protected String doInBackground(String... params) {
				
			List<NameValuePair> fields = new ArrayList<NameValuePair>();
			fields.add(new BasicNameValuePair("msg", vid));
			
			TransmissionServiceHandler svs = new TransmissionServiceHandler();
			String response = svs.makeServiceCall(url+"get/cmdty/vndr", POST, fields);
			
			return response;
		}
		
		@Override
        protected void onPostExecute(String json) {			
			dialog.dismiss();
			subs = Arrays.asList(gson.fromJson(json, Dto_GetXSubmission[].class));			
			CommodityAdapter adapter =  new  CommodityAdapter(ViewCommoditiesActivity.this);				
			
				for(int i = 0; i < subs.size(); i++){
	                adapter.add(new CommodityBody(subs.get(i).getSubmsn().getCmdty().getCommodity().getName(), 
	                		subs.get(i).getId(), subs.get(i).getSubmsn().getPrice(), 
	                		subs.get(i).getSubmsn().getMkt().getMarket().getName(), 
	                		subs.get(i).getSubmsn().getUnit()));
	            }
							
			setListAdapter(adapter);
		}
		
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.ADD_M) {
			startActivity(new Intent(ViewCommoditiesActivity.this, AddMktActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.ADD_C) {
			startActivity(new Intent(ViewCommoditiesActivity.this, AddCmdtyActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.ADD_P) {
			startActivity(new Intent(ViewCommoditiesActivity.this, AddPriceActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.VIEW) {
			startActivity(new Intent(ViewCommoditiesActivity.this, ViewVendorsActivity.class));
			finish();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean haveInternet(Context ctx) {
		// TODO Auto-generated method stub
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
           return false;
        }
        return true;
	}
	
	private void Alert(String title, String msg){
		AlertDialog.Builder pop = new AlertDialog.Builder(this);
		pop.setTitle(title);
		pop.setMessage(msg);
		pop.setPositiveButton("Settings", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), 0);		
			    onPause();
			}
		});
		pop.setNegativeButton("Cancel", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		pop.show();
   }
}
