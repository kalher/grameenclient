package com.vendorapp.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.vendorapp.dtos.Dto_GetVendorData;
import com.vendorapp.dtos.Dto_VendorData;
import com.vendorapp.net.Connectable;
import com.vendorapp.net.TransmissionServiceHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddMktActivity extends Activity implements Connectable{
	
	EditText market;
	Button submit;
	String mkt;
	Gson gson = new Gson();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_market);
		setTitle("Add Market");
	
		build();
		
	}
	
	public void build(){
		setUpViews();
		setClickers();
	}
	
	public void setUpViews(){
		market = (EditText)findViewById(R.id.MNAME);
		submit = (Button)findViewById(R.id.MKT_BTN);
	}
	
	public void setClickers(){
		
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mkt = market.getText().toString();
				
				if(haveInternet(AddMktActivity.this))					
					new Submitor().execute();
				else
					Alert("No Connection", "No internet connection. Make sure you are connected to the internet");				
				
				
			}
		});
		
	}

    private class Submitor extends AsyncTask<String, String, String> {
    	
    	ProgressDialog dialog;
    	
    	@Override
    	protected void onPreExecute(){
    	     dialog = ProgressDialog.show(AddMktActivity.this, "Submiting","Wait...", true);
    	        //do initialization of required objects objects here                
    	};
         
		@Override
		protected String doInBackground(String... params) {			
			
			List<NameValuePair> fields = new ArrayList<NameValuePair>();
			fields.add(new BasicNameValuePair("msg", mkt));
			
			TransmissionServiceHandler svs = new TransmissionServiceHandler();
			String response = svs.makeServiceCall(url+"add/mkt", POST, fields);
					
			return response;
		}
		
		@Override
        protected void onPostExecute(String json) {			
			dialog.dismiss();	
			Toast.makeText(AddMktActivity.this, json, Toast.LENGTH_LONG).show();
			
			market.setText("");
			
		}
		
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();		
		if (id == R.id.ADD_C) {
			startActivity(new Intent(AddMktActivity.this, AddCmdtyActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.ADD_P) {
			startActivity(new Intent(AddMktActivity.this, AddPriceActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.VIEW) {
		    startActivity(new Intent(AddMktActivity.this, ViewVendorsActivity.class));
		    finish();
		    return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean haveInternet(Context ctx) {
		// TODO Auto-generated method stub
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
           return false;
        }
        return true;
	}
	
	private void Alert(String title, String msg){
		AlertDialog.Builder pop = new AlertDialog.Builder(this);
		pop.setTitle(title);
		pop.setMessage(msg);
		pop.setPositiveButton("Settings", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), 0);		
			    onPause();
			}
		});
		pop.setNegativeButton("Cancel", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		pop.show();
   }
	
}
