package com.vendorapp.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;
import com.vendorapp.dtos.Dto_GetCommodity;
import com.vendorapp.dtos.Dto_GetMarket;
import com.vendorapp.dtos.Dto_GetVendorData;
import com.vendorapp.dtos.Dto_VendorData;
import com.vendorapp.net.Connectable;
import com.vendorapp.net.TransmissionServiceHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddPriceActivity extends Activity implements Connectable{
	
	EditText price, unit, vname;
	Spinner market, cmdty;
	List<Dto_GetCommodity> cmdtys;
	List<Dto_GetMarket> mkts;
	Button submit;
	String msg;
	Gson gson = new Gson();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
		setTitle("Add Price");
	
		build();
		
	}
	
	public void build(){
		
		new Mkts().execute();
		new Cmdty().execute();
		
		setUpViews();
		setClickers();
		
	}
	
	public void setUpViews(){
		market = (Spinner)findViewById(R.id.ADD_MARKET);
		cmdty = (Spinner)findViewById(R.id.ADD_COMMODITY);
		vname = (EditText)findViewById(R.id.ADD_VNAME);
		price = (EditText)findViewById(R.id.ADD_PRICE);
		unit = (EditText)findViewById(R.id.ADD_UNIT);
		submit = (Button)findViewById(R.id.ADD_BTN);
	}
	
	public void setClickers(){
		
		submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Dto_VendorData add = new Dto_VendorData();
				add.setCommodity(cmdtys.get(cmdty.getSelectedItemPosition()).getId());
				add.setMarket(mkts.get(market.getSelectedItemPosition()).getId());
				add.setPrice(price.getText().toString());
				add.setUnit(unit.getText().toString());
				add.setVname(vname.getText().toString());
				
				msg = gson.toJson(add);
				
				if(haveInternet(AddPriceActivity.this))
					new Submitor().execute();
				else
					Alert("No Connection", "No internet connection. Make sure you are connected to the internet");				

				
			}
		});
		
	}

    private class Submitor extends AsyncTask<String, String, String> {
    	
    	ProgressDialog dialog;
    	
    	@Override
    	protected void onPreExecute(){
    	     dialog = ProgressDialog.show(AddPriceActivity.this, "Submiting","Wait...", true);
    	        //do initialization of required objects objects here                
    	};
         
		@Override
		protected String doInBackground(String... params) {
							
			List<NameValuePair> fields = new ArrayList<NameValuePair>();
			fields.add(new BasicNameValuePair("msg", msg));
			
			TransmissionServiceHandler svs = new TransmissionServiceHandler();
			String response = svs.makeServiceCall(url+"add/submsn", POST, fields);
					
			return response;
		}
		
		@Override
        protected void onPostExecute(String json) {			
			dialog.dismiss();
			
			Toast.makeText(AddPriceActivity.this, json, Toast.LENGTH_LONG).show();
			
		}
		
    }
    
    private class Mkts extends AsyncTask<String, String, String> {
    	
    	ProgressDialog dialog;
    	
    	@Override
    	protected void onPreExecute(){
    	     dialog = ProgressDialog.show(AddPriceActivity.this, "Get Markets","Wait...", true);
    	        //do initialization of required objects objects here                
    	};
         
		@Override
		protected String doInBackground(String... params) {
							
			TransmissionServiceHandler svs = new TransmissionServiceHandler();
			String response = svs.makeServiceCall(url+"get/mkt", GET);
					
			return response;
		}
		
		@Override
        protected void onPostExecute(String json) {			
			dialog.dismiss();
			mkts = Arrays.asList(gson.fromJson(json, Dto_GetMarket[].class));
			List<String> markets = new ArrayList<String>(mkts.size());
			for(int i = 0; i < mkts.size(); i++){
				markets.add(mkts.get(i).getMarket().getName());
			}
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddPriceActivity.this, android.R.layout.simple_spinner_dropdown_item, markets);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			market.setAdapter(adapter);
		}
		
    }

    private class Cmdty extends AsyncTask<String, String, String> {
    	
    	ProgressDialog dialog;
    	
    	@Override
    	protected void onPreExecute(){
    	     dialog = ProgressDialog.show(AddPriceActivity.this, "Get Commodities","Wait...", true);
    	        //do initialization of required objects objects here                
    	};
         
		@Override
		protected String doInBackground(String... params) {
							
			TransmissionServiceHandler svs = new TransmissionServiceHandler();
			String response = svs.makeServiceCall(url+"get/cmdty", GET);
					
			return response;
		}
		
		@Override
        protected void onPostExecute(String json) {			
			dialog.dismiss();
			cmdtys = Arrays.asList(gson.fromJson(json, Dto_GetCommodity[].class));
			List<String> comms = new ArrayList<String>(cmdtys.size());
			for(int i = 0; i < cmdtys.size(); i++){
				comms.add(cmdtys.get(i).getCommodity().getName());
			}
			
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddPriceActivity.this, android.R.layout.simple_spinner_dropdown_item, comms);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			cmdty.setAdapter(adapter);
		}
		
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.ADD_M) {
			startActivity(new Intent(AddPriceActivity.this, AddMktActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.ADD_C) {
			startActivity(new Intent(AddPriceActivity.this, AddCmdtyActivity.class));
			finish();
			return true;
		}
				
		if (id == R.id.VIEW) {
		    startActivity(new Intent(AddPriceActivity.this, ViewVendorsActivity.class));
		    finish();
		    return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean haveInternet(Context ctx) {
		// TODO Auto-generated method stub
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
           return false;
        }
        return true;
	}
	
	private void Alert(String title, String msg){
		AlertDialog.Builder pop = new AlertDialog.Builder(this);
		pop.setTitle(title);
		pop.setMessage(msg);
		pop.setPositiveButton("Settings", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), 0);		
			    onPause();
			}
		});
		pop.setNegativeButton("Cancel", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		pop.show();
   }
	
}
