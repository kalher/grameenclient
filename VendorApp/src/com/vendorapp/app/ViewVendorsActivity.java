package com.vendorapp.app;

import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.vendorapp.dtos.Dto_GetVendor;
import com.vendorapp.net.Connectable;
import com.vendorapp.net.TransmissionServiceHandler;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ViewVendorsActivity extends ListActivity implements Connectable{

	Gson gson = new Gson();
	List<Dto_GetVendor> vendors;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_vendors);
		setTitle("Vendors");
		
		if(haveInternet(this))
			load();
		else
			Alert("No Connection", "No internet connection. Make sure you are connected to the internet");				

		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Intent out = new Intent(ViewVendorsActivity.this, ViewCommoditiesActivity.class);
		out.putExtra("vid", vendors.get(position).getId());
	    startActivity(out);
	
	}

	private void load(){
		new VendorLoader().execute();
	}
	
	private class VendorBody {
		public String name, id;
				
		public VendorBody(String name, String id) {
			this.name = name;
			this.id = id;		
		}
		
	}
	
	public class VendorAdapter extends ArrayAdapter<VendorBody> {

		public VendorAdapter(Context context) {
			super(context, 0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.vendor_lay, null);
			}
						
			TextView name = (TextView) convertView.findViewById(R.id.VNAME_LAY);
			name.setText(getItem(position).name);
			
			return convertView;
		}
	}
	
    private class VendorLoader extends AsyncTask<String, String, String> {
    	
    	ProgressDialog dialog;
    	
    	@Override
    	protected void onPreExecute(){
    	     dialog = ProgressDialog.show(ViewVendorsActivity.this, "Loading","Wait...", true);
    	        //do initialization of required objects objects here                
    	};
         
		@Override
		protected String doInBackground(String... params) {
				
			TransmissionServiceHandler svs = new TransmissionServiceHandler();
			String response = svs.makeServiceCall(url+"get/vndr", GET);
			
			return response;
		}
		
		@Override
        protected void onPostExecute(String json) {			
			dialog.dismiss();
		    
			vendors = Arrays.asList(gson.fromJson(json, Dto_GetVendor[].class));			
			VendorAdapter adapter =  new  VendorAdapter(ViewVendorsActivity.this);				
			
				for(int i = 0; i < vendors.size(); i++){
	                adapter.add(new VendorBody(vendors.get(i).getVendor().getName(), vendors.get(i).getId()));
	            }
							
			setListAdapter(adapter);
		}
		
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.ADD_M) {
			startActivity(new Intent(ViewVendorsActivity.this, AddMktActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.ADD_C) {
			startActivity(new Intent(ViewVendorsActivity.this, AddCmdtyActivity.class));
			finish();
			return true;
		}
		
		if (id == R.id.ADD_P) {
			startActivity(new Intent(ViewVendorsActivity.this, AddPriceActivity.class));
			finish();
			return true;
		}
				
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean haveInternet(Context ctx) {
		// TODO Auto-generated method stub
		NetworkInfo info = (NetworkInfo) ((ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
           return false;
        }
        return true;
	}
	
	private void Alert(String title, String msg){
		AlertDialog.Builder pop = new AlertDialog.Builder(this);
		pop.setTitle(title);
		pop.setMessage(msg);
		pop.setPositiveButton("Settings", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				startActivityForResult(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS), 0);		
			    onPause();
			}
		});
		pop.setNegativeButton("Cancel", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		pop.show();
   }
}
