package com.vendorapp.net;

import android.content.Context;

public interface Connectable {

	int GET = 1;
    int POST = 2;
    
    String url = "http://192.168.98.1:8080/VendorServer/";
    
    public boolean haveInternet(Context ctx);
}
