/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;


public class Dto_XSubmission {
    
    Dto_GetCommodity cmdty;
    Dto_GetMarket mkt;
    Dto_GetVendor vndr;
    String price;
    String unit;

    public Dto_XSubmission() {
    }

    public Dto_XSubmission(Dto_GetCommodity cmdty, Dto_GetMarket mkt, Dto_GetVendor vndr, String price, String unit) {
        this.cmdty = cmdty;
        this.mkt = mkt;
        this.vndr = vndr;
        this.price = price;
        this.unit = unit;
    }

    public Dto_GetCommodity getCmdty() {
        return cmdty;
    }

    public void setCmdty(Dto_GetCommodity cmdty) {
        this.cmdty = cmdty;
    }

    public Dto_GetMarket getMkt() {
        return mkt;
    }

    public void setMkt(Dto_GetMarket mkt) {
        this.mkt = mkt;
    }

    public Dto_GetVendor getVndr() {
        return vndr;
    }

    public void setVndr(Dto_GetVendor vndr) {
        this.vndr = vndr;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    
}
