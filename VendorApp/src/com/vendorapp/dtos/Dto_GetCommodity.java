/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_GetCommodity {
    private String id;
    private Dto_AddCommodity commodity;

    public Dto_GetCommodity() {
    }

    public Dto_GetCommodity(String id, Dto_AddCommodity commodity) {
        this.id = id;
        this.commodity = commodity;
    }

    public String getId() {
        return id;
    }

    public Dto_AddCommodity getCommodity() {
        return commodity;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCommodity(Dto_AddCommodity commodity) {
        this.commodity = commodity;
    }
    
    
}
