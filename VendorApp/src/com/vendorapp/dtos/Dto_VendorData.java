/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_VendorData {
    
    private String  market,
                    commodity,
                    price,
                    unit,
                    name;

    public Dto_VendorData() {
    }

    public Dto_VendorData(String market, String commodity, String price, String unit, String vname) {
        this.market = market;
        this.commodity = commodity;
        this.price = price;
        this.unit = unit;
        this.name = vname;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }
    
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setVname(String vname) {
        this.name = vname;
    }

    @Override
    public String toString() {
        return "Dto_VendorData{" + "market=" + market + ", commodity=" + commodity + ", price=" + price + ", unit=" + unit + ", name=" + name + '}';
    }
    
}
