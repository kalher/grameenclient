/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_GetMarket {
    private String id;
    private Dto_AddMarket market;

    public Dto_GetMarket() {
    }

    public Dto_GetMarket(String id, Dto_AddMarket market) {
        this.id = id;
        this.market = market;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dto_AddMarket getMarket() {
        return market;
    }

    public void setMarket(Dto_AddMarket market) {
        this.market = market;
    }
    
    

}
