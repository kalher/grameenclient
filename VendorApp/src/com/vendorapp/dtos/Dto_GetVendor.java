/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_GetVendor {
    private String id;
    private  Dto_AddVendor Vendor;

    public Dto_GetVendor() {
    }

    public Dto_GetVendor(String id, Dto_AddVendor Vendor) {
        this.id = id;
        this.Vendor = Vendor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dto_AddVendor getVendor() {
        return Vendor;
    }

    public void setVendor(Dto_AddVendor Vendor) {
        this.Vendor = Vendor;
    }
    
    
            
    
}
