/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_GetVendorData {
    
    public String id;
    public Dto_VendorData data;

    public Dto_GetVendorData(String id, Dto_VendorData data) {
        this.id = id;
        this.data = data;
    }

    public Dto_GetVendorData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dto_VendorData getData() {
        return data;
    }

    public void setData(Dto_VendorData data) {
        this.data = data;
    }
    
    
    
}
