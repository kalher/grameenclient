/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_AddVendor {
    
    private String name;

    public Dto_AddVendor() {
    }

    public Dto_AddVendor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

     
}
