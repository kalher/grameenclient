/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vendorapp.dtos;

/**
 *
 * @author hbt
 */
public class Dto_GetXSubmission {
    
    public String id;
    public Dto_XSubmission submsn;

    public Dto_GetXSubmission(String id, Dto_XSubmission submsn) {
        this.id = id;
        this.submsn = submsn;
    }

    public Dto_GetXSubmission() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dto_XSubmission getSubmsn() {
        return submsn;
    }

    public void setSubmsn(Dto_XSubmission submsn) {
        this.submsn = submsn;
    }
    
    
}
